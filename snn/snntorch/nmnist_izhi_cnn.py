import torch.nn.functional as F
import csv
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import IzhikevichParameter, IzhikevichState, surrogate
from snntorch import functional as SF
import torchmetrics
from tqdm import tqdm, trange
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np
from tonic import CachedDataset
import pytorch_lightning as pl
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.NMNIST.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                 time_window=5,
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=sensor_size,
        #    tau=2000,
        #    decay='exp'
        #    )
    ]
)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
first_saccade_only = False

trainset = tonic.datasets.NMNIST(
    save_to='../../data',
    first_saccade_only=first_saccade_only,
    transform=transform,
    train=True)
testset = tonic.datasets.NMNIST(
    save_to='../../data',
    transform=transform,
    first_saccade_only=first_saccade_only,
    train=False)

#Frames
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_test')

cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/nmnist_TF_tw5_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/nmnist_TF_tw5_test')

# #Timesurfaces
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/nmnist_TTS_f10_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/nmnist_TTS_f10_test')

# train samples: 70000, test samples: 10000
BATCH_SIZE = 32
train_subset_len = 1000
test_subset_len = 100

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=2)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          num_workers=2)

EPOCHS = 25
IN_FEATURES = 34
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 16
N_FILTERS_3 = 32
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 1
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = (str(IN_FEATURES) + 'x' + str(IN_FEATURES) + 'x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))

lr = 0.002
dt = 0.5
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
num_inputs = np.product(sensor_size)
num_outputs = len(trainset.classes)
slope = 25
spike_grad = surrogate.fast_sigmoid(slope=slope)  # slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)

# loss_fn = SF.mse_count_loss()
# loss_fn = SF.ce_rate_loss()
# loss_fn = SF.ce_count_loss()
# loss_fn = SF.mse_membrane_loss()
loss_fn = SF.ce_max_membrane_loss()
log_softmax = nn.LogSoftmax(dim=1)

params = IzhikevichParameter(a=0.01,
                             b=0.25,
                             c=-65,
                             d=2,
                             bias=140,
                             v_thres=30)

train_losses = []
test_losses = []
train_acc = []
test_acc = []


class SNN(pl.LightningModule):
    def __init__(self, num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features):
        super(SNN, self).__init__()
        self.mp = max_pooling
        self.conv1 = nn.Conv2d(
            num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = nn.Conv2d(
            num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = nn.Conv2d(
            num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.izhi1 = snn.Izhikevich(spike_grad=spike_grad, p=params, dt=dt)
        self.izhi2 = snn.Izhikevich(spike_grad=spike_grad, p=params, dt=dt)
        self.izhi3 = snn.Izhikevich(spike_grad=spike_grad, p=params, dt=dt)
        self.izhi4 = snn.Izhikevich(spike_grad=spike_grad, p=params, dt=dt)
        self.fc1 = nn.Linear(dense_features, output_features)

        self.train_acc = torchmetrics.Accuracy()
        self.test_acc = torchmetrics.Accuracy()

    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        x = x.reshape(seq_len, batch, -1)
        s1 = s2 = s3 = so = IzhikevichState(
            v=torch.tensor(-65.0, requires_grad=True, device=device),
            u=torch.tensor(-65.0, device=device) * params.b)
        voltages = []

        for ts in range(seq_len):
            z = x[ts, :, :]
            cur1 = self.conv1(z)
            spk1, s1 = self.izhi1(cur1, s1)
            z = F.max_pool2d(spk1, self.mp[0])
            cur2 = self.conv2(z)
            spk2, s2 = self.izhi2(cur2, s2)
            z = F.max_pool2d(spk2, self.mp[1])
            cur3 = self.conv3(z)
            spk3, s3 = self.izhi3(cur3, s3)
            cur4 = self.fc1(spk3.view(batch, -1))
            spk4, so = self.izhi4(cur4, so)
            voltages.append(so[0])
        return torch.stack(voltages)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=lr)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        x, _ = torch.max(output, 0)
        log_p_y = log_softmax(x)
        train_acc = self.train_acc(log_p_y, target)
        loss = loss_fn(output, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        x, _ = torch.max(output, 0)
        log_p_y = log_softmax(x)
        test_acc = self.test_acc(log_p_y, target)
        loss = loss_fn(output, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN(num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
            kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
            num_channels=N_CHANNELS,
            max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
            dense_features=DENSE_FEATURES,
            output_features=OUTPUT_FEATURES)

trainer = pl.Trainer(gpus=1, strategy="ddp",
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_sanity_val_steps=0,
                     num_nodes=1)

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time

test_losses = test_losses[1:]
test_acc = test_acc[1:]

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Test Accuracy{test_acc[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [train_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_acc)
plt.plot(test_acc)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_acc, test_acc])

filename_raw = 'snntorch_izhi_results_raw.csv'
filename_pretty = 'snntorch_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' loss function',
          ' spike gradient',
          ' v_th',
          ' slope',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        device,
        runtime,
        (train_acc[-1], test_acc[-1]),
        (train_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        params.v_thres,
        slope,
        params.a,
        params.b,
        params.c,
        params.d,
        params.bias,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
