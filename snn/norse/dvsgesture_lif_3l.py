import pytorch_lightning as pl
import csv
import datetime
import time
from prettytable.prettytable import from_csv
import torchmetrics
from tqdm import tqdm
from typing import NamedTuple
from norse.torch import LICell, LIState
from norse.torch.module.lif import LIFCell
from norse.torch import LIFParameters, LIFState
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
import torch.nn as nn
import torch.nn.functional as F
import sys
from tonic import CachedDataset
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.DVSGesture.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001, spatial_factor=1),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.DVSGesture.sensor_size,
                                 time_window=10,
                                 ),
        # tonic.transforms.Denoise(filter_time=1000),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.DVSGesture.sensor_size,
        #                                 tau=5000,
        #                                 decay='exp'
        #                                 )
    ]
)


trainset = tonic.datasets.DVSGesture(
    save_to='../../data', transform=transform, train=True)
testset = tonic.datasets.DVSGesture(
    save_to='../../data',  transform=transform, train=False)

#Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/dvsgesture_TF_tw10_train')
cached_dataset_test = CachedDataset(
    testset, cache_path='../../data/cached_datasets/dvsgesture_TF_tw10_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/dvsgesture_TTS_f1000_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/dvsgesture_TTS_f1000_test')

# train samples: 1077, test samples: 264
BATCH_SIZE = 8
train_subset_len = 1077
test_subset_len = 264

train_subset = torch.utils.data.random_split(
    cached_dataset_train, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_dataset_test, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=4)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=False,
                                          num_workers=4)

neuron_params = LIFParameters(
    alpha=torch.as_tensor(100),
    tau_syn_inv=torch.as_tensor(1.0/1e-2),
    tau_mem_inv=torch.as_tensor(1.0/5e-3),
    v_th=torch.as_tensor(0.5),
    v_leak=torch.as_tensor(0.0),
    v_reset=torch.as_tensor(0.0),
    method='super'
)

EPOCHS = 25
LR = 0.002
dt = 1e-3
INPUT_FEATURES = np.product(sensor_size)
HIDDEN_FEATURES_L1 = 100
HIDDEN_FEATURES_L2 = 100
HIDDEN_FEATURES_L3 = 100
OUTPUT_FEATURES = len(trainset.classes)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
layers = (HIDDEN_FEATURES_L1, HIDDEN_FEATURES_L2)
train_losses = []
test_losses = []
train_acc = []
test_acc = []


class SNN(pl.LightningModule):
    def __init__(self):
        super(SNN, self).__init__()
        self.fc_in = torch.nn.Linear(INPUT_FEATURES, HIDDEN_FEATURES_L1)
        self.fc_out = torch.nn.Linear(HIDDEN_FEATURES_L1, HIDDEN_FEATURES_L2)
        self.fc_out2 = torch.nn.Linear(HIDDEN_FEATURES_L2, HIDDEN_FEATURES_L3)
        self.fc_out3 = torch.nn.Linear(HIDDEN_FEATURES_L3, OUTPUT_FEATURES)
        self.lif1 = LIFCell(p=neuron_params, dt=dt)
        self.lif2 = LIFCell(p=neuron_params, dt=dt)
        self.lif3 = LIFCell(p=neuron_params, dt=dt)
        self.out = LICell(dt=dt)

        self.train_acc = torchmetrics.Accuracy()
        self.test_acc = torchmetrics.Accuracy()

    def forward(self, x):
        seq_length, batch, _, _, _ = x.shape
        x = x.reshape(seq_length, batch, -1)
        s1 = so = s2 = s3 = None
        voltages = []
        for ts in range(seq_length):
            z = x[ts, :, :]
            z = self.fc_in(z)  # Input -> Hidden_L1
            z, s1 = self.lif1(z, s1)  # LIF
            z = self.fc_out(z)  # Hidden_L1 -> Hidden_L2
            z, s2 = self.lif2(z, s2)  # LIF
            z = self.fc_out2(z)  # Hidden_L2 -> Hidden_L3
            z, s3 = self.lif3(z, s3)  # LIF
            z = self.fc_out3(z)  # Hidden -> Output
            vo, so = self.out(z, so)  # LICell Output
            voltages += [vo]  # LIF
        x, _ = torch.max(torch.stack(voltages), 0)
        log_p_y = torch.nn.functional.log_softmax(x, dim=1)
        return log_p_y

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=LR)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        train_acc = self.train_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        test_acc = self.test_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN()
trainer = pl.Trainer(gpus=1, strategy="ddp",
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_nodes=1,
                     num_sanity_val_steps=0)

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[-1] = tf_str[-1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Test Accuracy{test_acc[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [train_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_acc)
plt.plot(test_acc)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_acc, test_acc])

filename_raw = 'norse_lif_results_raw.csv'
filename_pretty = 'norse_lif_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' optimizer',
          ' layers, n_spikes',
          ' method',
          ' alpha',
          ' tau_mem_inv',
          ' tau_syn_inv',
          ' v_leak',
          ' v_reset',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        DEVICE,
        runtime,
        (train_acc[-1], test_acc[-1]),
        (train_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers, 
        neuron_params.method,
        neuron_params.alpha.item(),
        neuron_params.tau_mem_inv.item(),
        neuron_params.tau_syn_inv.item(),
        neuron_params.v_leak.item(),
        neuron_params.v_reset.item(),
        neuron_params.v_th.item(),
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
