import torch.nn.functional as F
import torch.nn as nn
import pytorch_lightning as pl
import csv
import datetime
import sys
import time
from prettytable.prettytable import from_csv
import torchmetrics
from norse.torch.module.izhikevich import IzhikevichCell
from norse.torch.functional.izhikevich import IzhikevichSpikingBehavior, IzhikevichState, IzhikevichParameters
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
from tonic import CachedDataset
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.DVSGesture.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001, spatial_factor=1),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.DVSGesture.sensor_size,
                                 time_window=10,
                                 ),
        # tonic.transforms.Denoise(filter_time=1000),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.DVSGesture.sensor_size,
        #                                 tau=5000,
        #                                 decay='exp'
        #                                 )
    ]
)

trainset = tonic.datasets.DVSGesture(
    save_to='../../data', transform=transform, train=True)
testset = tonic.datasets.DVSGesture(
    save_to='../../data',  transform=transform, train=False)

#Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/dvsgesture_TF_tw10_train')
cached_dataset_test = CachedDataset(
    testset, cache_path='../../data/cached_datasets/dvsgesture_TF_tw10_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/dvsgesture_TTS_f1000_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/dvsgesture_TTS_f1000_test')

# train samples: 1077, test samples: 264
BATCH_SIZE = 4
train_subset_len = 1077
test_subset_len = 264

train_subset = torch.utils.data.random_split(
    cached_dataset_train, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_dataset_test, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=2)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=False,
                                          num_workers=2)

p_izhi = IzhikevichParameters(a=0.01,
                              b=0.25,
                              c=-65,
                              d=2,
                              bias=140,
                              v_th=30,
                              tau_inv=500,
                              method='super',
                              alpha=100)

spiking_behavior = IzhikevichSpikingBehavior(
    p=p_izhi,
    s=IzhikevichState(
        v=torch.tensor(-65.0, requires_grad=True),
        u=torch.tensor(-65) * p_izhi.b
    )
)

EPOCHS = 25
LR = 0.002
dt = 1e-3
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
IN_FEATURES = 128
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 16
N_FILTERS_3 = 32
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 1
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = (str(IN_FEATURES) + 'x' + str(IN_FEATURES) + 'x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))
train_losses = []
test_losses = []
train_acc = []
test_acc = []

class SNN(pl.LightningModule):
    def __init__(self, num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features):
        super(SNN, self).__init__()
        self.mp = max_pooling
        self.conv1 = nn.Conv2d(num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = nn.Conv2d(num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = nn.Conv2d(num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.izhi1 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.izhi2 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.izhi3 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.fc1 = nn.Linear(dense_features, output_features)
        self.izhi_out = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        
        self.train_acc = torchmetrics.Accuracy()
        self.test_acc = torchmetrics.Accuracy()

    def forward(self, x):
        seq_length, batch, _, _, _ = x.shape
        x = x.reshape(seq_length, batch, -1)
        s1 = so = s2 = s3 = None
        voltages = []

        for ts in range(seq_length):
            z = x[ts, :, :]
            z = self.conv1(z)
            z, s1 = self.izhi1(z, s1)
            z = F.max_pool2d(z, self.mp[0])
            z = self.conv2(z)
            z, s2 = self.izhi2(z, s2)
            z = F.max_pool2d(z, self.mp[1])
            z = self.conv3(z)
            z, s3 = self.izhi3(z, s3)
            z = self.fc1(z.view(batch, -1))
            z, so = self.izhi_out(z, so)
            voltages += [so[0]]  # IZHI
        x, _ = torch.max(torch.stack(voltages), 0)
        log_p_y = torch.nn.functional.log_softmax(x, dim=1)
        return log_p_y

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=LR)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        train_acc = self.train_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        test_acc = self.test_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN(num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
            kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
            num_channels=N_CHANNELS,
            max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
            dense_features=DENSE_FEATURES,
            output_features=OUTPUT_FEATURES)

model = SNN()
trainer = pl.Trainer(gpus=1, strategy="ddp",
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_nodes=1,
                     num_sanity_val_steps=0)

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[-1] = tf_str[-1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Test Accuracy{test_acc[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [train_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_acc)
plt.plot(test_acc)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_acc, test_acc])

filename_raw = 'norse_izhi_results_raw.csv'
filename_pretty = 'norse_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers, n_spikes',
          ' method',
          ' alpha',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' tau_inv',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        DEVICE,
        runtime,
        (train_acc[-1], test_acc[-1]),
        (train_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        p_izhi.method,
        p_izhi.alpha,
        p_izhi.a,
        p_izhi.b,
        p_izhi.c,
        p_izhi.d,
        p_izhi.bias,
        p_izhi.tau_inv,
        p_izhi.v_th,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
