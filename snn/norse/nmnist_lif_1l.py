import csv
import datetime
import time
from prettytable.prettytable import from_csv
import torchmetrics
from norse.torch import LICell, LIState
from norse.torch.module.lif import LIFCell
from norse.torch import LIFParameters, LIFState
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
import sys
from tonic import CachedDataset
import pytorch_lightning as pl
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.NMNIST.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.NMNIST.sensor_size,
                                 time_window=1,
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.NMNIST.sensor_size,
        #    tau=2000,
        #    decay='exp'
        #    )
    ]
)

first_saccade_only = False
trainset = tonic.datasets.NMNIST(
    save_to='../../data',
    transform=transform,
    first_saccade_only=first_saccade_only,
    train=True)
testset = tonic.datasets.NMNIST(
    save_to='../../data',
    transform=transform,
    first_saccade_only=first_saccade_only,
    train=False)

#Frames
cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_test')

# #Timesurfaces
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/nmnist_TTS_f10_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/nmnist_TTS_f10_test')

BATCH_SIZE = 32

train_subset_len = 10000
test_subset_len = 1000

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=4)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          num_workers=4)

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

neuron_params = LIFParameters(
    alpha=torch.as_tensor(100),
    tau_mem_inv=torch.as_tensor(1.0/5e-3),
    tau_syn_inv=torch.as_tensor(1.0/2e-2),
    v_th=torch.as_tensor(0.5),
    v_leak=torch.as_tensor(0.0),
    v_reset=torch.as_tensor(0.0),
    method='super'
)

LR = 0.002
EPOCHS = 10
INPUT_FEATURES = np.product(trainset.sensor_size)
HIDDEN_FEATURES = 100
OUTPUT_FEATURES = len(trainset.classes)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
layers = (HIDDEN_FEATURES)
dt = 1e-3


train_losses = []
test_losses = []
train_acc = []
test_acc = []


class SNN(pl.LightningModule):
    def __init__(self):
        super(SNN, self).__init__()
        self.fc_in = torch.nn.Linear(np.product(sensor_size), HIDDEN_FEATURES)
        self.fc_out = torch.nn.Linear(HIDDEN_FEATURES, OUTPUT_FEATURES)
        self.lif1 = LIFCell(p=neuron_params, dt=dt)
        self.out = LICell(dt=dt)

        self.train_acc = torchmetrics.Accuracy()
        self.test_acc = torchmetrics.Accuracy()

    def forward(self, x):
        seq_length, batch, _, _, _ = x.shape
        x = x.reshape(seq_length, batch, -1)
        s1 = so = None
        voltages = []
        for ts in range(seq_length):
            z = x[ts, :, :]
            z = self.fc_in(z)  # Input -> Hidden
            z, s1 = self.lif1(z, s1)  # LIF
            z = self.fc_out(z)  # Hidden -> Output
            vo, so = self.out(z, so)  # LI Output
            voltages += [vo]  # Output
        x, _ = torch.max(torch.stack(voltages), 0)
        log_p_y = torch.nn.functional.log_softmax(x, dim=1)
        return log_p_y

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=LR)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        train_acc = self.train_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        test_acc = self.test_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN()
trainer = pl.Trainer(gpus=1, strategy="ddp",
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_nodes=1,
                     num_sanity_val_steps=0)

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Test Accuracy{test_acc[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [train_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_acc)
plt.plot(test_acc)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_acc, test_acc])

filename_raw = 'norse_nmnist_lif_results_raw.csv'
filename_pretty = 'norse_nmnist_lif_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' method',
          ' alpha',
          ' tau_mem_inv',
          ' tau_syn_inv',
          ' v_leak',
          ' v_reset',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        DEVICE,
        runtime,
        (train_acc[-1], test_acc[-1]),
        (train_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        neuron_params.method,
        neuron_params.alpha.item(),
        neuron_params.tau_mem_inv.item(),
        neuron_params.tau_syn_inv.item(),
        neuron_params.v_leak.item(),
        neuron_params.v_reset.item(),
        neuron_params.v_th.item(),
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
