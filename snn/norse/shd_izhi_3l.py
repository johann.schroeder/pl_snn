import pytorch_lightning as pl
import csv
import datetime
import sys
import time
from prettytable.prettytable import from_csv
import torchmetrics
from norse.torch.module.izhikevich import IzhikevichCell
from norse.torch.functional.izhikevich import IzhikevichSpikingBehavior, IzhikevichState, IzhikevichParameters
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
from tonic import CachedDataset
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.SHD.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.SHD.sensor_size,
                                 time_window=5)
    ]
)

trainset = tonic.datasets.SHD(save_to='../../data',
                              transform=transform,
                              train=True)

testset = tonic.datasets.SHD(save_to='../../data',
                             transform=transform,
                             train=False)

#Frames
cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/shd_TF_tw5_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/shd_TF_tw5_test')

# train samples: 8156, test samples: 2264
train_subset_len = 8156
test_subset_len = 2264

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

BATCH_SIZE = 64

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=2)

test_loader = torch.utils.data.DataLoader(test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          num_workers=2)

p_izhi = IzhikevichParameters(a=0.01,
                              b=0.25,
                              c=-65,
                              d=2,
                              v_th=30,
                              tau_inv=500,
                              method='super',
                              alpha=100)

spiking_behavior = IzhikevichSpikingBehavior(
    p=p_izhi,
    s=IzhikevichState(
        v=torch.tensor(-65.0, requires_grad=True),
        u=torch.tensor(-65) * p_izhi.b
    )
)

EPOCHS = 100
LR = 0.002
dt = 1e-3
INPUT_FEATURES = np.product(sensor_size)
HIDDEN_FEATURES_L1 = 100
HIDDEN_FEATURES_L2 = 100
HIDDEN_FEATURES_L3 = 100
OUTPUT_FEATURES = len(trainset.classes)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
layers = (HIDDEN_FEATURES_L1, HIDDEN_FEATURES_L2, HIDDEN_FEATURES_L3)
train_losses = []
test_losses = []
train_acc = []
test_acc = []


class SNN(pl.LightningModule):
    def __init__(self):
        super(SNN, self).__init__()
        self.fc_in = torch.nn.Linear(INPUT_FEATURES, HIDDEN_FEATURES_L1)
        self.fc_out = torch.nn.Linear(HIDDEN_FEATURES_L1, HIDDEN_FEATURES_L2)
        self.fc_out2 = torch.nn.Linear(HIDDEN_FEATURES_L2, HIDDEN_FEATURES_L3)
        self.fc_out3 = torch.nn.Linear(HIDDEN_FEATURES_L3, OUTPUT_FEATURES)
        self.izhi1 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.izhi2 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.izhi3 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.izhi_out = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)

        self.train_acc = torchmetrics.Accuracy()
        self.test_acc = torchmetrics.Accuracy()

    def forward(self, x):
        seq_length, batch, _, _ = x.shape
        x = x.reshape(seq_length, batch, -1)
        s1 = so = s2 = s3 = None
        voltages = []
        for ts in range(seq_length):
            z = x[ts, :, :]
            z = self.fc_in(z)  # Input -> Hidden_L1
            z, s1 = self.izhi1(z, s1)  # IZHIKEVICH
            z = self.fc_out(z)  # Hidden_L1 -> Hidden_L2
            z, s2 = self.izhi2(z, s2)  # IZHIKEVICH
            z = self.fc_out2(z)  # Hidden_L2 -> Hidden_L3
            z, s3 = self.izhi3(z, s3)  # IZHIKEVICH
            z = self.fc_out3(z)  # Hidden_L3 -> Output
            vo, so = self.izhi_out(z, so)  # Izhikevich Output
            voltages += [so[0]]  # LIF

        x, _ = torch.max(torch.stack(voltages), 0)
        log_p_y = torch.nn.functional.log_softmax(x, dim=1)
        return log_p_y

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=LR)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        train_acc = self.train_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        test_acc = self.test_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN()
trainer = pl.Trainer(gpus=1, strategy="ddp",
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_nodes=1,
                     num_sanity_val_steps=0)

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[-1] = tf_str[-1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Test Accuracy{test_acc[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [train_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_acc)
plt.plot(test_acc)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_acc, test_acc])

filename_raw = 'norse_izhi_results_raw.csv'
filename_pretty = 'norse_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers, n_spikes',
          ' method',
          ' alpha',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' tau_inv',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        DEVICE,
        runtime,
        (train_acc[-1], test_acc[-1]),
        (train_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        p_izhi.method,
        p_izhi.alpha,
        p_izhi.a,
        p_izhi.b,
        p_izhi.c,
        p_izhi.d,
        p_izhi.bias,
        p_izhi.tau_inv,
        p_izhi.v_th,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
