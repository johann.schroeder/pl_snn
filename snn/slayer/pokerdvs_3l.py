from tonic import CachedDataset
import tonic
from torch.utils.data import Dataset, DataLoader
import torch
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from slayerPytorch.src.learningStats import learningStats
from prettytable.prettytable import from_csv
import gc
import time
import csv
import slayerSNN as snn
import sys
import os
CURRENT_TEST_DIR = os.getcwd()
gc.collect()
plt.style.use('science')
torch.cuda.empty_cache()
sensor_size = tonic.datasets.POKERDVS.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                 time_window=1,
                                 ),
        # tonic.transforms.Denoise(filter_time=100),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
        #    tau=1000,
        #    decay='exp'
        #    )
    ]
)

BATCH_SIZE = 8
trainset = tonic.datasets.POKERDVS(
    save_to='../data', train=True, transform=transform)
testset = tonic.datasets.POKERDVS(
    save_to='../data', train=False, transform=transform)
#Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../data/cached_datasets/pokerdvs_TF_tw1_train')
cached_dataset_test = CachedDataset(
    testset, cache_path='../data/cached_datasets/pokerdvs_TF_tw1_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/pokerdvs_TTS_f100_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../data/cached_datasets/pokerdvs_TTS_f100_test')
EPOCHS = 25
lr = 0.002
input_size = np.product(tonic.datasets.POKERDVS.sensor_size)
hidden_size_l1 = 100
hidden_size_l2 = 100
hidden_size_l3 = 100
n_classes = len(trainset.classes)
layers = (hidden_size_l1, hidden_size_l2, hidden_size_l3)

netParams = snn.params('network_pokerdvs.yaml')
# Network definition


class Network(torch.nn.Module):
    def __init__(self, netParams):
        super(Network, self).__init__()
        # Initialize slayer
        slayer = snn.layer(netParams['neuron'], netParams['simulation'])
        self.slayer = slayer
        # Define network functions
        # The commented line below should be used if the input spikes were not reshaped
        self.fc1 = slayer.dense(sensor_size, hidden_size_l1)
        self.fc2 = slayer.dense(hidden_size_l1, hidden_size_l2)
        self.fc3 = slayer.dense(hidden_size_l2, hidden_size_l3)
        self.fc4 = slayer.dense(hidden_size_l3, n_classes)

    def forward(self, spikeInput):
        # Both set of definitions are equivalent. The uncommented version is much faster.
        # spikeLayer1 = self.slayer.spike(self.fc1(spikeInput))
        # spikeLayer2 = self.slayer.spike(self.fc2(spikeLayer1))
        spikeLayer1 = self.slayer.spike(self.slayer.psp(self.fc1(spikeInput)))
        spikeLayer2 = self.slayer.spike(self.slayer.psp(self.fc2(spikeLayer1)))
        spikeLayer3 = self.slayer.spike(self.slayer.psp(self.fc3(spikeLayer2)))
        spikeLayer4 = self.slayer.spike(self.slayer.psp(self.fc4(spikeLayer3)))

        return spikeLayer4
        # return spikeInput, spikeLayer1, spikeLayer2


if __name__ == '__main__':
    # Define the cuda device to run the code on.
    device = torch.device('cuda')

    # Create network instance.
    net = Network(netParams).to(device)

    # Create snn loss instance.
    error = snn.loss(netParams).to(device)

    # Define optimizer module.
    optimizer = torch.optim.Adam(net.parameters(), lr=lr)

    # Dataset and dataLoader instances.
    train_loader = torch.utils.data.DataLoader(dataset=trainset,
                                               batch_size=BATCH_SIZE,
                                               collate_fn=tonic.collation.PadTensors(),
                                               shuffle=False,
                                               )
    test_loader = torch.utils.data.DataLoader(dataset=testset,
                                              batch_size=BATCH_SIZE,
                                              collate_fn=tonic.collation.PadTensors(),
                                              shuffle=False
                                              )

    # Learning stats instance.
    stats = learningStats()
    start_time = time.time()

    # Main loop
    for epoch in range(EPOCHS):
        tSt = datetime.now()
        for i, (events, label) in enumerate(iter(train_loader), 0):
            target = torch.zeros((len(label), len(trainset.classes), 1, 1, 1))
            input = events.permute([1, 2, 3, 4, 0])

            for idx, l in enumerate(label):
                target[idx, l.item(), ...] = 1

            # Move the input and target to correct GPU.
            input = input.float().to(device)
            target = target.to(device)

            # Forward pass of the network.
            output = net.forward(input)

            # Gather the training stats.
            stats.training.correctSamples += torch.sum(
                snn.predict.getClass(output) == label).data.item()
            stats.training.numSamples += len(label)

            # Calculate loss.
            loss = error.numSpikes(output, target)

            # Reset gradients to zero.
            optimizer.zero_grad()

            # Backward pass of the network.
            loss.backward()

            # Update weights.
            optimizer.step()

            # Gather training loss stats.
            stats.training.lossSum += loss.cpu().data.item()

            # Display training stats.
            stats.print(epoch, i, (datetime.now() - tSt).total_seconds())

        # Testing loop.
        # Same steps as Training loops except loss backpropagation and weight update.
        for i, (events, label) in enumerate(iter(test_loader), 0):
            target = torch.zeros((len(label), len(testset.classes), 1, 1, 1))
            input = events.permute([1, 2, 3, 4, 0])

            for idx, l in enumerate(label):
                target[idx, l.item(), ...] = 1
            input = input.float().to(device)
            target = target.to(device)
            # print(input.shape, target.shape, label.shape)
            output = net.forward(input)
            stats.testing.correctSamples += torch.sum(
                snn.predict.getClass(output) == label).data.item()
            stats.testing.numSamples += len(label)

            loss = error.numSpikes(output, target)
            stats.testing.lossSum += loss.cpu().data.item()
            stats.print(epoch, i)

        # Update stats.
        stats.update()

    runtime = time.time() - start_time
    date = datetime.now()
    filetype = '.png'
    d = str(date.day) + "_" + str(date.month) + "_" + \
        str(date.year) + "_" + str(date.time())
    d = d.split('.')[0]
    scriptname = sys.argv[0].split('.')[0]
    tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
    try:
        tf_str[1] = tf_str[1].replace(",decay='exp'", "")
    except:
        pass
    opt = str(optimizer).replace(
        " ", "").strip().splitlines(keepends=False)[2:-1]
    # Plot the results.
    # Learning loss
    fig = plt.figure(facecolor="w", figsize=(10, 5))
    plt.semilogy(stats.training.lossLog, label='Training')
    plt.semilogy(stats.testing.lossLog, label='Testing')
    plt.title('Loss Curves')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()

    plt.savefig("./pokerdvs/plots/" + scriptname +
                "/loss" + "_" + d + filetype)

    np.save("./pokerdvs/plots/" + scriptname + "/loss_data" +
            "_" + d + '.npy', [stats.training.lossLog, stats.testing.lossLog])

    # Learning accuracy
    fig = plt.figure(facecolor="w", figsize=(10, 5))
    plt.plot(stats.training.accuracyLog, label='Training')
    plt.plot(stats.testing.accuracyLog, label='Testing')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.savefig("./pokerdvs/plots/" + scriptname +
                "/acc" + "_" + d + filetype)

    np.save("./pokerdvs/plots/" + scriptname + "/acc_data" +
            "_" + d + '.npy', [stats.training.accuracyLog, stats.testing.accuracyLog])

    filename_raw = 'slayer_pokerdvs_results_raw.csv'
    filename_pretty = 'slayer_pokerdvs_results_pretty.csv'

    fields = ['n',
              ' date',
              ' filename',
              ' device',
              ' runtime',
              ' accuracy (train, test)',
              ' loss (train, test)',
              ' epochs',
              ' batch_size',
              ' opt',
              ' layers',
              ' tSample',
              ' type',
              ' theta',
              ' tauSr',
              ' tauRef',
              ' scaleRef',
              ' tauRho',
              ' scaleRho',
              ' tgtSpikeRegion',
              ' tgtSpikeCount',
              ' transform']

    num_rows = 0
    for r in open(filename_raw):
        num_rows += 1
    if num_rows != 0:
        num_rows -= 1

    rows = [[num_rows,
            d.replace('_', ' '),
            sys.argv[0].split('.')[0] + '.py',
            device,
            runtime,
            (stats.training.accuracyLog[-1], stats.testing.accuracyLog[-1]),
            (stats.training.lossLog[-1], stats.testing.lossLog[-1]),
            EPOCHS,
            BATCH_SIZE,
            opt,
            layers,
            netParams['simulation']['tSample'],
            netParams['neuron']['type'],
            netParams['neuron']['theta'],
            netParams['neuron']['tauSr'],
            netParams['neuron']['tauRef'],
            netParams['neuron']['scaleRef'],
            netParams['neuron']['tauRho'],
            netParams['neuron']['scaleRho'],
            netParams['training']['error']['tgtSpikeRegion'],
            netParams['training']['error']['tgtSpikeCount'],
            tf_str]]

    with open(filename_raw, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        if num_rows == 0:
            csvwriter.writerow(fields)
        csvwriter.writerows(rows)

    with open(filename_raw) as fp:
        table = from_csv(fp)

    f = open(filename_pretty, "w")
    f.write(table.get_string())
    f.close()
