import tonic
from torch.utils.data import Dataset, DataLoader
import torch
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from slayerPytorch.src.learningStats import learningStats
from prettytable.prettytable import from_csv
import gc
import time
import csv
import slayerSNN as snn
import sys
import os
from tonic import CachedDataset
CURRENT_TEST_DIR = os.getcwd()
sys.path.append(CURRENT_TEST_DIR + "/../../src")
gc.collect()
plt.style.use('science')
torch.cuda.empty_cache()
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.NMNIST.sensor_size,
                                 time_window=1,
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.NMNIST.sensor_size,
        #    tau=5000,
        #    decay='exp'
        #    )
    ]
)

BATCH_SIZE = 16
first_saccade_only=False

trainset = tonic.datasets.NMNIST(
    save_to='../data', 
    train=True, 
    transform=transform,
    first_saccade_only=first_saccade_only)
testset = tonic.datasets.NMNIST(
    save_to='../data', 
    train=False, 
    transform=transform,
    first_saccade_only=first_saccade_only)

#Frames
cached_trainset = CachedDataset(
    trainset, cache_path='../data/cached_datasets/nmnist_TF_tw1_train')
cached_testset = CachedDataset(
    testset, cache_path='../data/cached_datasets/nmnist_TF_tw1_test')

# #Timesurfaces
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/nmnist_TTS_f10_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/nmnist_TTS_f10_test')

# train samples: 70000, test samples: 10000
train_subset_len = 70000
test_subset_len = 10000

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

# Dataset and dataLoader instances.
train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=True)

input_size = np.product(tonic.datasets.NMNIST.sensor_size)
sensor_size = tonic.datasets.NMNIST.sensor_size
n_classes = len(trainset.classes)

EPOCHS = 10
lr = 0.002

IN_FEATURES = 34
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 12
N_FILTERS_3 = 16
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 2
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = ('35x35x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))
netParams = snn.params('network_pokerdvs.yaml')
# Network definition


class Network(torch.nn.Module):
    def __init__(self,
                 netParams,
                 num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features):
        super(Network, self).__init__()
        # Initialize slayer
        slayer = snn.layer(netParams['neuron'], netParams['simulation'])
        self.slayer = slayer
        self.mp = max_pooling
        # Define network functions
        # The commented line below should be used if the input spikes were not reshaped
        self.conv1 = slayer.conv(num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = slayer.conv(num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = slayer.conv(num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.pool1 = slayer.pool(self.mp[0])
        self.pool2 = slayer.pool(self.mp[1])
        self.fc1 = slayer.dense(dense_features, output_features)

    def forward(self, spikeInput):
        self.batch = spikeInput.shape[0]
        self.time = spikeInput.shape[-1]
        # Both set of definitions are equivalent. The uncommented version is much faster.
        # spikeLayer1 = self.slayer.spike(self.fc1(spikeInput))
        # spikeLayer2 = self.slayer.spike(self.fc2(spikeLayer1))
        spikeLayer1 = self.slayer.spike(self.conv1(self.slayer.psp(spikeInput)))
        spikeLayer2 = self.slayer.spike(self.pool1(self.slayer.psp(spikeLayer1)))
        spikeLayer3 = self.slayer.spike(self.conv2(self.slayer.psp(spikeLayer2)))
        spikeLayer4 = self.slayer.spike(self.pool2(self.slayer.psp(spikeLayer3)))
        spikeLayer5 = self.slayer.spike(self.conv3(self.slayer.psp(spikeLayer4)))
        spikeOut = self.slayer.spike(self.fc1(self.slayer.psp(spikeLayer5)))
        return spikeOut
        # return spikeInput, spikeLayer1, spikeLayer2
 
if __name__ == '__main__':      
    # Define the cuda device to run the code on.
    device = torch.device('cuda')

    # Create network instance.
    net = Network(num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
                  kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
                  num_channels=N_CHANNELS,
                  max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
                  dense_features=(cl3, cl3, N_FILTERS_3),
                  output_features=OUTPUT_FEATURES,
                  netParams=netParams).to(device)

    # Create snn loss instance.
    error = snn.loss(netParams).to(device)

    # Define optimizer module.
    optimizer = torch.optim.Adam(net.parameters(), lr = 0.002)

    # Learning stats instance.
    stats = learningStats()
    start_time = time.time()
    
    # Main loop
    for epoch in range(EPOCHS):
        tSt = datetime.now()
        for i, (events, label) in enumerate(iter(train_loader), 0):
            target = torch.zeros((len(label), len(trainset.classes), 1, 1, 1))
            input = events.permute([1, 2, 3, 4, 0])
            
            for idx, l in enumerate(label):
                target[idx, l.item(), ...] = 1

            # Move the input and target to correct GPU.
            input = input.to(device)
            target = target.to(device)
            # Forward pass of the network.
            output = net.forward(input)

            # Gather the training stats.
            stats.training.correctSamples += torch.sum(
                snn.predict.getClass(output) == label).data.item()
            stats.training.numSamples += len(label)
            
            # Calculate loss.
            loss = error.numSpikes(output, target)

            # Reset gradients to zero.
            optimizer.zero_grad()

            # Backward pass of the network.
            loss.backward()

            # Update weights.
            optimizer.step()

            # Gather training loss stats.
            stats.training.lossSum += loss.cpu().data.item()

            # Display training stats.
            stats.print(epoch, i, (datetime.now() - tSt).total_seconds())

        # Testing loop.
        # Same steps as Training loops except loss backpropagation and weight update.
        for i, (events, label) in enumerate(iter(test_loader), 0):
            target = torch.zeros((len(label), len(testset.classes), 1, 1, 1))
            input = events.permute([1, 2, 3, 4, 0])
            
            for idx, l in enumerate(label):
                target[idx, l.item(), ...] = 1
            input  = input.to(device)
            target = target.to(device) 
            # print(input.shape, target.shape, label.shape)
            output = net.forward(input)
            stats.testing.correctSamples += torch.sum( 
                snn.predict.getClass(output) == label ).data.item()
            stats.testing.numSamples     += len(label)

            loss = error.numSpikes(output, target)
            stats.testing.lossSum += loss.cpu().data.item()
            stats.print(epoch, i)
        
        # Update stats.
        stats.update()

    runtime = time.time() - start_time
    date = datetime.now()
    filetype = '.png'
    d = str(date.day) + "_" + str(date.month) + "_" + \
        str(date.year) + "_" + str(date.time())
    d = d.split('.')[0]
    scriptname = sys.argv[0].split('.')[0]
    tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
    try:
        tf_str[1] = tf_str[1].replace(",decay='exp'", "")
    except:
        pass
    opt = str(optimizer).replace(
        " ", "").strip().splitlines(keepends=False)[2:-1]
    # Plot the results.
    # Learning loss
    fig = plt.figure(facecolor="w", figsize=(10, 5))
    plt.semilogy(stats.training.lossLog, label='Training')
    plt.semilogy(stats.testing.lossLog, label='Testing')
    plt.title('Loss Curves')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()

    plt.savefig("./nmnist/plots/" + scriptname +
                "/loss" + "_" + d + filetype)

    np.save("./nmnist/plots/" + scriptname + "/loss_data" +
            "_" + d + '.npy', [stats.training.lossLog, stats.testing.lossLog])

    # Learning accuracy
    fig = plt.figure(facecolor="w", figsize=(10, 5))
    plt.plot(stats.training.accuracyLog, label='Training')
    plt.plot(stats.testing.accuracyLog, label='Testing')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.savefig("./nmnist/plots/" + scriptname +
                "/acc" + "_" + d + filetype)

    np.save("./nmnist/plots/" + scriptname + "/acc_data" +
            "_" + d + '.npy', [stats.training.accuracyLog, stats.testing.accuracyLog])

    filename_raw = 'slayer_nmnist_results_raw.csv'
    filename_pretty = 'slayer_nmnist_results_pretty.csv'

    fields = ['n',
              ' date',
              ' filename',
              ' device',
              ' runtime',
              ' accuracy (train, test)',
              ' loss (train, test)',
              ' epochs',
              ' batch_size',
              ' opt',
              ' layers',
              ' first saccade only',
              ' tSample',
              ' type',
              ' theta',
              ' tauSr',
              ' tauRef',
              ' scaleRef',
              ' tauRho',
              ' scaleRho',
              ' tgtSpikeRegion',
              ' tgtSpikeCount',
              ' transform']

    num_rows = 0
    for r in open(filename_raw):
        num_rows += 1
    if num_rows != 0:
        num_rows -= 1

    rows = [[num_rows,
            d.replace('_', ' '),
            sys.argv[0].split('.')[0] + '.py',
            device,
            runtime,
            (stats.training.accuracyLog[-1], stats.testing.accuracyLog[-1]),
            (stats.training.lossLog[-1], stats.testing.lossLog[-1]),
            EPOCHS,
            BATCH_SIZE,
            opt,
            layers,
            first_saccade_only,
            netParams['simulation']['tSample'],
            netParams['neuron']['type'],
            netParams['neuron']['theta'],
            netParams['neuron']['tauSr'],
            netParams['neuron']['tauRef'],
            netParams['neuron']['scaleRef'],
            netParams['neuron']['tauRho'],
            netParams['neuron']['scaleRho'],
            netParams['training']['error']['tgtSpikeRegion'],
            netParams['training']['error']['tgtSpikeCount'],
            tf_str]]

    with open(filename_raw, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        if num_rows == 0:
            csvwriter.writerow(fields)
        csvwriter.writerows(rows)

    with open(filename_raw) as fp:
        table = from_csv(fp)

    f = open(filename_pretty, "w")
    f.write(table.get_string())
    f.close()
