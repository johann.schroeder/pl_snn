import pytorch_lightning as pl
import tonic
from torch.utils.data import Dataset, DataLoader
import torch
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from slayerPytorch.src.learningStats import learningStats
from prettytable.prettytable import from_csv
import gc
import time
import csv
import slayerSNN as snn
import sys
import os
from tonic import CachedDataset
CURRENT_TEST_DIR = os.getcwd()
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001, spatial_factor=1),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.DVSGesture.sensor_size,
                                 time_window=10,
                                 ),
        # tonic.transforms.Denoise(filter_time=1000),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.DVSGesture.sensor_size,
        #                                 tau=5000,
        #                                 decay='exp'
        #                                 )
    ]
)


trainset = tonic.datasets.DVSGesture(
    save_to='../data', transform=transform, train=True)
testset = tonic.datasets.DVSGesture(
    save_to='../data',  transform=transform, train=False)

#Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../data/cached_datasets/dvsgesture_TF_tw10_train')
cached_dataset_test = CachedDataset(
    testset, cache_path='../data/cached_datasets/dvsgesture_TF_tw10_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/dvsgesture_TTS_f1000_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../data/cached_datasets/dvsgesture_TTS_f1000_test')

# train samples: 1077, test samples: 264
BATCH_SIZE = 8
train_subset_len = 1077
test_subset_len = 264

train_subset = torch.utils.data.random_split(
    cached_dataset_train, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_dataset_test, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=4)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=False,
                                          num_workers=4)

EPOCHS = 25
lr = 0.002
input_size = np.product(tonic.datasets.DVSGesture.sensor_size)
n_classes = len(trainset.classes)

IN_FEATURES = 128
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 16
N_FILTERS_3 = 32
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 2
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = (str(IN_FEATURES) + 'x' + str(IN_FEATURES) + 'x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))

netParams = snn.params('network_dvsgesture.yaml')
stats = learningStats()


class SNN(pl.LightningModule):
    def __init__(self, netParams, 
                 num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features):
        super(SNN, self).__init__()
        self.error = snn.loss(netParams)
        slayer = snn.layer(netParams['neuron'], netParams['simulation'])
        self.slayer = slayer
        self.mp = max_pooling
        self.conv1 = slayer.conv(num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = slayer.conv(num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = slayer.conv(num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.pool1 = slayer.pool(self.mp[0])
        self.pool2 = slayer.pool(self.mp[1])
        self.fc1 = slayer.dense(dense_features, output_features)

    def forward(self, x):
        x = x.permute([1, 2, 3, 4, 0])
        self.batch = x.shape[0]
        self.time = x.shape[-1]
        spikeLayer1 = self.slayer.spike(self.conv1(self.slayer.psp(x)))
        spikeLayer2 = self.slayer.spike(self.pool1(self.slayer.psp(spikeLayer1)))
        spikeLayer3 = self.slayer.spike(self.conv2(self.slayer.psp(spikeLayer2)))
        spikeLayer4 = self.slayer.spike(self.pool2(self.slayer.psp(spikeLayer3)))
        spikeLayer5 = self.slayer.spike(self.conv3(self.slayer.psp(spikeLayer4)))
        spikeOut = self.slayer.spike(self.fc1(self.slayer.psp(spikeLayer5)))
        return spikeOut

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=lr)
        return optimizer

    def training_step(self, batch, batch_idx):
        event, label = batch
        target = torch.zeros((len(label), len(trainset.classes), 1, 1, 1))
        # input = data.permute([1, 2, 3, 4, 0])
        for idx, l in enumerate(label):
            target[idx, l.item(), ...] = 1
        output = self(event)
        stats.training.correctSamples += torch.sum(
            snn.predict.getClass(output).cuda() == label).data.item()
        stats.training.numSamples += len(label)
        loss = self.error.numSpikes(output, target)
        stats.training.lossSum += loss.cpu().data.item()
        return loss

    def validation_step(self, batch, batch_idx):
        event, label = batch
        target = torch.zeros((len(label), len(trainset.classes), 1, 1, 1))
        for idx, l in enumerate(label):
            target[idx, l.item(), ...] = 1
        output = self(event)
        stats.testing.correctSamples += torch.sum(
            snn.predict.getClass(output).cuda() == label).data.item()
        stats.testing.numSamples += len(label)
        loss = self.error.numSpikes(output, target)
        stats.testing.lossSum += loss.cpu().data.item()
        return loss

    def validation_epoch_end(self, outputs):
        stats.update()


model = SNN(netParams,
            num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
            kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
            num_channels=N_CHANNELS,
            max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
            dense_features=(cl3, cl3, N_FILTERS_3),
            output_features=OUTPUT_FEATURES)

trainer = pl.Trainer(gpus=-1, strategy="dp",
                     max_epochs=EPOCHS,
                     log_every_n_steps=1)

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time

date = datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
# Plot the results.
# Learning loss
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.semilogy(stats.training.lossLog[1:], label='Training')
plt.semilogy(stats.testing.lossLog[1:], label='Testing')
plt.title('Loss Curves')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend()

plt.savefig("./dvsgesture/plots/" + scriptname +
            "/loss" + "_" + d + filetype)

np.save("./dvsgesture/plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [stats.training.lossLog[1:], stats.testing.lossLog[1:]])

# Learning accuracy
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(stats.training.accuracyLog[1:], label='Training')
plt.plot(stats.testing.accuracyLog[1:], label='Testing')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend()

plt.savefig("./dvsgesture/plots/" + scriptname +
            "/acc" + "_" + d + filetype)

np.save("./dvsgesture/plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [stats.training.accuracyLog[1:], stats.testing.accuracyLog[1:]])

filename_raw = 'slayer_dvsgesture_results_raw.csv'
filename_pretty = 'slayer_dvsgesture_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' tSample',
          ' type',
          ' theta',
          ' tauSr',
          ' tauRef',
          ' scaleRef',
          ' tauRho',
          ' scaleRho',
          ' tgtSpikeRegion',
          ' tgtSpikeCount',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        torch.device(0),
        runtime,
        (stats.training.accuracyLog[-1], stats.testing.accuracyLog[-1]),
        (stats.training.lossLog[-1], stats.testing.lossLog[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        netParams['simulation']['tSample'],
        netParams['neuron']['type'],
        netParams['neuron']['theta'],
        netParams['neuron']['tauSr'],
        netParams['neuron']['tauRef'],
        netParams['neuron']['scaleRef'],
        netParams['neuron']['tauRho'],
        netParams['neuron']['scaleRho'],
        netParams['training']['error']['tgtSpikeRegion'],
        netParams['training']['error']['tgtSpikeCount'],
        tf_str]]

with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
