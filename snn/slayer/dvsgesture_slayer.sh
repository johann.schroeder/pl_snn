#!/bin/bash

#Job parameters
#SBATCH --job-name=my_job
#SBATCH --output=./%x_%j.out
#SBATCH --error=./%x_%j.err
#SBATCH --mail-user=joschroeder@techfak.uni-bielefeld.de --mail-type=FAIL

#Resources
#SBATCH --time=03:00:00
#SBATCH --partition=volta
#SBATCH --qos=compute_gpu
#SBATCH --ntasks=8
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --gpus-per-task=8

source $HOME/senv/bin/activate

echo -e "Start $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log start time etc.

#Job step(s)
srun --nodes=1 python -u dvsgesture_1l.py 

echo -e "End $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log end time etc.
